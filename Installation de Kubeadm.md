# Installation du cluster k8s via kubeadm sur un 

Ajouter repository + lock maj auto (via exclude)

```sh
sudo su && cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF
```

```sh
sudo setenforce 0
sudo swapoff -a
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
```

Installer kubelet sur toute les nodes et ses dépendances

```sh
sudo yum install -y conntrack socat kubelet-1.18.2 ebtables --disableexclude=kubernetes
```
```
sudo su && cat <<EOF > /etc/sysctl.conf
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl -p
```
)
## Pour tester sur une seule vm l'installation de kubernetes:
```
sudo kubeadm init --pod-network-cidr=10.241.72.64/26 --kubernetes-version=v1.18.2
```

## Pour installer le vrai cluster (haute dispo):
### Installation du premier master:
On initialise notre 1er master en pointant le load balancer externe qui va se charger de répartir les charges entre les différents masters sur le port 6443. Cette adresse est importante, car c'est cette dernière qui va être utilisé par les autres master pour rejoindre le cluster (avec kubeadm join).
```
sudo kubeadm init --control-plane-endpoint "LOAD_BALANCER_DNS:LOAD_BALANCER_PORT" --pod-network-cidr=10.244.0.0/16 --upload-certs --kubernetes-version=v1.18.2
```
Ici l'ip du loadbalancer est: 10.241.100.7 ce qui donne:
```
sudo kubeadm init --control-plane-endpoint "10.241.100.7:6443" --pod-network-cidr=10.244.0.0/16 --upload-certs --kubernetes-version=v1.18.2
```
### Installation de Calico
Il faut installer un gestionnaire de réseau sur K8S nous avons choisi Calico car c'est le plus répendu et celui installer par Google sur sa solution GKE. L'installation passe par l'application de ce fichier yaml : (il peut être télécharger si l'environnement ne permet pas l'accès au site projectcalico.org)
```
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
```
#### Subtilité maison
Le port utilisé par calico est le port BGP (179) (Border gateway protocol) il faut donc l'ouvrir sur toutes les machines (voir section 4). Une autre subtilité est que sur nos VMs nous avons deux interfaces réseau: l'interface de production et l'interface d'administration qui est celle par défaut. Calico par défaut va utiliser notre interface d'administration celle ci n'a pas les ports ouverts ni les flux ça ne fonctionnera pas pour forcer l'interface réseau utiliser voici la commande:
```
kubectl set env daemonset/calico-node -n kube-system IP_AUTODETECTION_METHOD=interface=ens192
```
Il faudra remplacer le nom de l'interface au besoin

### Installation des master restant

kubeadm join --token <token> <master-ip>:<master-port> --discovery-token-ca-cert-hash sha256:<hash>
#### Subtilité maison
Comme expliqué pour Calico nous avons deux interfaces, Kubelet dans son comportement d'origine va utiliser l'interface par défaut (ici l'interface d'administration) pour modifier le comportement il faut préciser à kubelet l'interface à utiliser. Pour se faire il faut repréciser l'ip la machine (sur chacune d'entre elle) en ajoutant `--node-ip=<IPPRODUCTIONMACHINE>` à la commande du fichier `/var/lib/kubelet/kubeadm-flags.env` ce qui fait passer le contenu de fichier de:
```
KUBELET_KUBEADM_ARGS="--cgroup-driver=cgroupfs --network-plugin=cni --pod-infra-container-image=k8s.gcr.io/pause:3.2"
```
à
```
KUBELET_KUBEADM_ARGS="--node-ip=<IPPRODUCTIONMACHINE> --cgroup-driver=cgroupfs --network-plugin=cni --pod-infra-container-image=k8s.gcr.io/pause:3.2"
```
Redémarrez ensuite kubelet:
```
sudo systemctl daemon-reload && sudo  systemctl restart kubelet
```

De base les masters empèchent tout déploiement de pod en leur seins, de fait on autorise ça à travers:
```
kubectl taint nodes --all node-role.kubernetes.io/master-
```

### Installation des workers
kubeadm join  <master-ip>:<master-port> --token <token> --discovery-token-ca-cert-hash <hash>

## Configuration de Kubectl
Auto-complétion:
```
source /etc/profile.d/bash_completion.sh  
echo "source <(kubectl completion bash)" >> ~/.bashrc

```
Executer en user non-root

```sh
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

permettre aux masters d'accueillir des pods

```sh
kubectl taint nodes --all node-role.kubernetes.io/master-
```

## NFS
 helm install --namespace kube-system --set storageClass.defaultClass=true --set replicaCount=4 --set nfs.server=10.241.72.82 --set nfs.path=/partage nfs-client-provisioner .


quay.io/external_storage/nfs-client-provisioner:v3.1.0-k8s1.11
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: PVC_NAME
spec:
  storageClassName: standard
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi # Taille

problème storage mkdir permission denied 

## Ingress
repo: https://github.com/helm/charts/tree/master/stable/nginx-ingress
mettre les replicas à 4
 helm install ingress-controller --namespace=kube-system -f values.yaml .

controller.service.enabled=false
kind=DaemonSet
userHostPort = true
kubectl label nodes idfmc2pk8s01 ingress-controller-node="true"
kubectl label nodes idfmc2pk8s02 ingress-controller-node="true"
kubectl label nodes idfmmapk8s03 ingress-controller-node="true"
kubectl label nodes idfmmapk8s04 ingress-controller-node="true"

kubectl label nodes idfmc2pk8s01 ingress-controller-node-
kubectl label nodes idfmc2pk8s02 ingress-controller-node-
kubectl label nodes idfmmapk8s03 ingress-controller-node-
kubectl label nodes idfmmapk8s04 ingress-controller-node-
      nodeSelector: { "node-role.kubernetes.io/master": ""}


# HOW TO

## Supprimer un noeud

délier du cluster

```sh
kubectl delete no idfmc2pk8s01
```

se positionner sur le noeud

```sh
kubeadm reset -f
```
