# GUIDE UTILISATEUR 

Ce document a pour but de guider l’utilisateur sur la forge, depuis la création d’un repo jusqu’à la mise à disposition de l’application, et en passant par l’intégration continue. 


## PRÉSENTATION DE LA FORGE 

### OUTILS : 

La forge logicielle est un ensemble d’outil permettant d’aider à adopter la méthodologie DevOps. Elle se compose de plusieurs outils interconnectés simplifiant la vie des équipes de développement. 

Les 5 outils de bases sont : 

Un gestionnaire de source è Nous avons choisi Gitlab, simple d’utilisation et très complet il permet une grande flexibilité lors des différentes phases de développement. 
Il est basé sur Git et permet d’organiser le travail avec des branches et des tags 

Un outil de Qualimétrie è SonarQube est l’outil qui a été choisi ici. Il permet d’analyser et de donner une note à l’application en fonction de plusieurs critères : 

Découverte de bugs 

Analyse des bonnes pratiques 

Le code coverage (pourcentage du code testé) 

Analyse de certaines failles de sécurité 

Un outil d’intégration continue è Nous avons choisi Jenkins. Cet outil est le leader du marché, de plus, il dispose d’une plus grosse communauté. C’est celui qui permet la plus grande flexibilité. 

Un outil de déploiement continue è Nous avons choisi Argocd, il est parfaitement adapté au déploiement continue sur Kubernetes. 

Un gestionnaire d’artefacts è Nexus a été choisi car il permet de gérer un grand nombre de types d’artefacts. Il est notamment capable d’héberger une registry docker qui va permettre à l’outil d’intégration continue, une fois qu’il aura construit les images d’aller les pousser sur ce registry. 


## ORCHESTRATEUR 

Nous avons choisi d’utiliser un orchestrateur pour offrir un socle moderne et flexible à la forge logicielle IDFM. 

 

## COMMENT LES OUTILS SONT-ILS INTERCONNECTÉS ? 





 


 

# GITLAB 

## CRÉATION D’UN REPOSITORY SUR GITLAB 

La première étape pour tout projet est la création d’un repository pour centraliser ce code, et ainsi pouvoir l’exploiter avec Jenkins et Argo. 

En accédant ici, on arrive sur GitLab : https://gitlab.opendata-iledefrance.fr/ 

Se connecter avec vos identifiants : Une image contenant capture d’écran

Description générée automatiquement 

Une fois connecté, allez sur Project --> Your project --> Puis le projet qui vous est destiné. 

 

 

On crée un sous-groupe : 

 

Puis on remplit le nom du sous-groupe avec le nom de votre application. 

 

 

Une fois cela fait, créer deux projets avec le nom “Deploy” et “Application” : 

 

Puis remplir le champ « Project Name » 

 

Puis le champ description (optionnel). 



## CLONER LE REPOSITORY 

 

NOTE : Le gitlab est accessible depuis le https. Vous ne pourrez pas utiliser SSH pour effectuer des commits ou pousser des sources. 

Pour cloner le repository on utilise la commande : 

```git
$ git clone https://sources.forge.iledefrance-mobilites.fr/mon-application/microservice1.git 
```


Cette commande permet de cloner le repo sur votre poste.

Par défaut la commande clone va cloner le projet dans un dossier portant son nom ici ce sera microservice1.

Tapez « cd microservice1 » pour y accéder. 

 

## POUSSER DANS LE REPOSITORY 

Vous pouvez donc maintenant créer plusieurs fichiers. Une fois les différents fichiers de votre application prêtes. 

 

Voici les commandes permettant de pousser vos fichiers vers le repository : 

Pour commit le change en local :

```git
$ git add . 
```
 

Ensuite mettre une description de votre commit :  

```git
$ git commit -m "ajout de la fonctionnalité d’authentification" 
```
 

Pour le pousser sur le repository en ligne : 

```git
$ git push 
```

NOTE : pour le premier push, il faut ajouter à la commande ces arguments : -u origin master 

 

##COMMENT PRÉPARER LES FICHIERS D’INTÉGRATION CONTINUE 

Afin de préparer les repo à l’utilisation de Jenkins nous aurons besoin dans chaque repo qui va utiliser l’intégration continue il faut placer un Jenkinsfile. 

Le Jenkinsfile de base pour une application dockerisé avec un frontend et un backend est le suivant : 

```groovy
#!/usr/bin/env groovy  

def  appName = 'YOUR_APP_NAME'  

def worker = "${appName}-application-worker"  

def registryBase = "artefacts.forge.iledefrance-mobilites.fr"  

def branch = env.BRANCH_NAME  

def backend = 'backend'  

def version = ''  

def releaseName = ${appName}-${branch}  

 

pipeline {  

  agent {  

    kubernetes {  

        label worker  

        defaultContainer 'default'  

        yaml """  

            apiVersion: v1  

            kind: Pod  

            metadata:  

            labels:  

              component: ci  

            spec:  

              # Use service account that can deploy to all namespaces  

              serviceAccountName: default  

              containers:  

              - name: sonar-scanner  

                image: newtmitch/sonar-scanner:3.0.3-alpine  

                env:  

                - name: SONAR_SCANNER_OPTS  

                  value: "-Xmx1024m"  

                resources:  

                  limits:  

                    cpu: "1"  

                    memory: 1300Mi  

                  requests:  

                    cpu: "0.5"  

                    memory: 500Mi  

                command:  

                - cat  

                tty: true   

              - name: argocd  

                image: payfit/argocd-cli  

                command:  

                - cat  

                tty: true  

              - name: docker  

                image: docker:18.03.1-ce  

                securityContext:  

                  privileged: true  

                volumeMounts:  

                - mountPath: /var/run/docker.sock  

                  name: docker-sock-volume  

                - name: config-json  

                  mountPath: /root/.docker/config.json  

                  subPath: config.json  

                command:  

                - cat  

                tty: true  

              volumes:  

              - name: docker-sock-volume  

                hostPath:  

                  # location on host  

                  path: /var/run/docker.sock  

                  # this field is optional  

                  type: File  

              - name: config-json  

                secret:  

                  secretName: regcred  

                  items:  

                  - key: .dockerconfigjson  

                    path: config.json  

            """  

    }  

  }  

    stages {  

        stage('Analyse du code SonarQube') {  

            steps {  

                container("sonar-scanner") {  

                    script {  

                        pwd = sh(returnStdout: true, script: 'pwd').trim()  

                        sh "sonar-scanner -Dsonar.projectKey=egt -Dsonar.projectBaseDir='${pwd}' -Dsonar.projectName='${appName}' -Dsonar.sources='frontend' -Dsonar.host.url=http://sonarqube.tools.svc.cluster.local:9000 -Dsonar.login=763b3dfbe5cf158ceacc67c783419b6029b9188e"  

                    }  

                }  

            }  

        }   

        stage('Versionning du build') {  

            steps {  

                    script {  

                        version = readFile('VERSION').trim()  

                        def versions = version.split('\\.')  

                        def major = versions[0]  

                        def minor = versions[0] + '.' + versions[1]  

                    }  
            }  

        }  

        stage('Build Docker Images') {  

            parallel{  

                stage('Build BackEnd') {  

                    steps {  

                        container('docker')  

                        {sh "docker build -t ${registryBase}/${appName}/backend:${version} ./backend"}      

                    }  

                }  
                stage('Build FrontEnd') {  

                    steps {  

                        container('docker')  

                        {sh "docker build -t ${registryBase}/${appName}/frontend:${version} ./frontend"}      

                    }  

                }  

            }  

        }  

        stage('Push Docker Images') {  

            parallel{  

                stage('Push BackEnd') {  

                    steps {  

                        container('docker')  

                        {sh "docker push ${registryBase}/${appName}/backend:${version}"}      

                    }  

                }  
                stage('Push FrontEnd') {  

                    steps {  

                        container('docker')  

                        {sh "docker push ${registryBase}/${appName}/frontend:${version}"}      

                    }  

                }  

            }  

        }  

        stage("Anchore CVE analysis") {  

            steps {  

                container("docker") {  

                     sh "echo \"${registryBase}/${appName}/backend:${version} ${pwd}/backend/Dockerfile\" >> anchore_images"  

                     sh "echo \"${registryBase}/${appName}/frontend:${version} ${pwd}/frontend/Dockerfile\" >> anchore_images"  

                     sh "cat anchore_images"  

                     anchore bailOnFail: false, name: 'anchore_images', forceAnalyze: true, engineRetries: "600"  

                    }  

            }  

        }   

    stage("argocd mise à jour") {  

      steps {  

        container("argocd") {  

          withCredentials([string(credentialsId: "argocd-secret", variable: 'ARGOCD_AUTH_TOKEN')]) {  

              script {  

                  sh """  

                  echo ${version}  

                  export ARGOCD_SERVER='deploiement.forge.iledefrance-mobilites.fr';  

                  export APP_NAME=${releaseName};  

                  argocd --grpc-web --insecure app set \$APP_NAME -p global.backend.image.tag=${version} -p global.frontend.image.tag=${version};  

                  argocd --insecure --grpc-web app sync \$APP_NAME --force;  

                  """  

              }  

          }  

        }  

      }  

    }  

    }  

} 
```
Exemple à adapter d’un pipeline 

 

Vous devrais certainement adapter ce fichier à vos besoins. 

Pour plus de détails vous pouvez consulter la documentation utilisateur Jenkins au besoin. 

Pour le dossier Deploy contenant le chart Helm de l’application voici un exemple de Jenkinsfile : 

```groovy
#!/usr/bin/env groovy 

 

def  appName = 'YOURAPPLICATIONNAME' 

def worker = "${appName}-worker" 

def registryBase = "artefacts.forge.iledefrance-mobilites.fr" 

def branch = env.BRANCH_NAME 

def backend = 'backend' 

def version = '' 

 

pipeline { 

  agent { 

    kubernetes { 

        label worker 

        defaultContainer 'default' 

        yaml """ 

            apiVersion: v1 

            kind: Pod 

            metadata: 

            labels: 

              component: ci 

            spec: 

              # Use service account that can deploy to all namespaces 

              serviceAccountName: default 

              containers: 

              - name: helm 

                image: openstackmagnum/helm-client:v3.2.0 

                command: 

                - cat 

                tty: true 

            """ 

    } 

  } 
```
   
```groovy
    stages { 

          stage('Upload helm chart') { 

            steps { 

                container("helm") { 

                  withCredentials([string(credentialsId: "nexus-secret", variable: 'NEXUS_SECRET')]) { 

                        script { 

                            sh ''' 

                            helm dependency update charts/egt; 

                            HELM_PACKAGE=$(helm package charts/egt | awk -F'[ ]' '{print $8}') 

                            curl --insecure -v --upload-file $HELM_PACKAGE -u admin:$NEXUS_SECRET https://artefacts.forge.iledefrance-mobilites.fr/repository/helm-idfm/ 

                            ''' 

                        } 

                    }    

                } 

            } 

        } 

    } 

 

} 
```

Ce dernier va packager votre chart helm et le pousser automatiquement sur le repo helm sur le nexus. Si vous avez un package différent (fichiers K8S par exemple) vous devrez adapter le fichier. 

Il est nécessaire d’ajouter un fichier Dockerfile (dans le cas de ce Jenkinsfile et du deploiement sur Kubernetes), contenant permettant de composer l’image. Voici un exemple minimaliste de ce à quoi doit ressembler un Dockerfile : 

```Dockerfile
# Pull base image. 

FROM nginx:latest 


# Define working directory. 

WORKDIR /etc/nginx 


# Define default command. 

CMD ["nginx"] 

 
# Expose ports. 

EXPOSE 80 443 
``` 
 
 
# JENKINS 

## CONNEXION JENKINS 

Pour se connecter à Jenkins, il faut se rendre à l’adresse suivante :  

https://jenkins.opendata-iledefrance.fr/ 

Lors de votre arriver, vous serez automatiquement redirigée vers la page de connexion du ADFS. 

Pour vous loger, il suffit de mettre nomutilisateur@opendata-iledefrance.fr , puis votre mot de passe 

 

 


 

## CRÉATION D’UN PROJET SUR JENKINS 

Maintenant, choisissez le bon répertoire selon votre affection TMA, ensuite vous pouvez créer le projet Jenkins : 

 

Pour créer un pipeline provenant d’un groupe GitLab, on va sur nouveau item : 

  

On saisit le nom du groupe du projet de GitLab :  

  

Puis sur Gitlab Group :  

  

Cliquez sur ok : 

 

 

Dans la partie Gitlab Group, Checkout Credential, on clique sur Ajouter, puis choisissez le nom de votre projet (ici “ idfm/tma/nomapplication/repositoryapplication ” dans owner)  

 

 

Puis remplir les champs “Nom d’utilisateur” et “Mot de passe” avec des accès ayant les droits suffisants sur le GitLab 

 

Cliquez sur Ajouter, puis Save 

  

## VOIR LE PIPELINE VIA BLUEOCEAN 

 

Jenkins possède une commuté actif, ce qui fait qu’on trouve de nombreux plugins, certain peuvent être totalement inutile, alors que d’autre peuvent apporter une véritable plu value. C’est le cas du plugin BlueOcean. 

BleuOcean permet d’avoir une meilleure visualisation des pipelines sur leur états pendant le build, un retour compartimenté de la console sur chaque étape et on peut connaitre le résultat du pipeline en un coup d’œil. 

Pour ouvrir BlueOcean, il suffit simplement de se rendre sur son projet, puis sur le côté gauche, cliquer sur BlueOcean  

On arrive ensuite sur la page d’accueil de BlueOcean.  

Cette interface nous montre rapidement les éléments importants, la santé, l’état, le nom de la branche, le commit, le dernier message et depuis quand ce pipeline est terminé, jusque-là rien de plus que sur jenkins 

 

 

Via cette page on retrouve plusieurs possibilités, on peut voir les activités du projet, les pulls requests. Lorsque l’on clique sur la ligne, on arrive au grand avantage de BleuOcean, la visualisation graphique du pipeline. 

 

Exemple pendant le fonctionnement du pipeline : 

 

Exemple pipeline en parallèle 1 

 

Exemple possibilité de voir la console 1 

## RAPPORT ANCORE 

Anchore permet de faire des tests de sécurité et ou de conformité des images docker. Le rapport est ensuite consultable sur jenkins via un plugin. 

Pour consulter ces rapports, il suffit de vous rendre au niveau de la branche de votre application, puis cliquer sur « anchore report »  

Zone de texteAvant de cliquer pour avoir les détails, sur la droite vous avez un aperçu du nombre d’alerte en fonction des builds effectué, ce qui permet de voir l’évolution entre chaque build Jenkins 

 

 

 

 

 

Exemple de rapport obtenu par Anchore : 

 

 

#SONARQUBE 

##SE CONNECTER À SONARQUBE 

Afin de pour jeter un œil sur la qualité du code, il faut se connecter à SonarQube via le lien : https://qualitecode.forge.iledefrance-mobilites.fr/ 

Vous serez automatiquement renvoyé sur la page de connexion, il suffit de vous connecter avec vos identifiants et votre mot de passe. 

 

Une fois loger, vous arrivez sur votre interface, ou vous voyez tous vos applications, pour plus de détail consulter la documentation utilisateur SonarQube 

 

 

 

#ARGOCD 

##SE CONNECTER À ARGOCD 

Dans votre Jenkinsfile, le stage « mise à jour argocd » va aller envoyer la commande à Argo de déployer votre application. 
Pour voir l’état de votre application sur le cluster vous pouvez utiliser argocd. Vous n’aurez accès qu’en lecture seule à l’outil depuis l’interface. 

Vous pouvez vous connecter à l’aide de l’adresse suivante : 

https://argo.opendata-iledefrance.fr/ vous pouvez ensuite cliquez sur le bouton « login with ldap » 

Entrez votre compte utilisateur Atos comme sur les autres outils 

Une image contenant capture d’écran

Vous aurez accès à l’interface  

Une image contenant capture d’écran

Cliquez sur votre application vous aurez alors l’état de santé de l’application (représenté par un cœur vert ou rouge) et également accès au détail du déploiement : 

Une image contenant capture d’écran, ordinateur


Pour plus d’information vous pouvez consulter la documentation utilisateur d’ArgoCD. 

# FINALISER LE PROJET 

On va ajouter 3 branches pour avoir un environnement dev, préprod et prod 

 

 

Nous allons créer les branches dev, pprod et prod pour cela on va le faire depuis le terminal de votre projet :  

```git
$ git checkout -b dev && \ 

   git add . && \ 

   git commit -m "init branch" && \ 

   git push -u origin dev 
```
Ensuite du dev on crée une branche pprod 
```git
$ git checkout –b pprod && \ 

   git add . && \ 

   git commit -m "init branch" && \ 

   git push -u origin pprod 
```
Puis de la pprod on crée une branche prod 
```1
$ git checkout –b prod && \ 

   git add . && \ 

   git commit -m "init branch" && \ 

   git push -u origin prod 
```


#NEXUS 

##SE CONNECTER À NEXUS 

L’url de connexion est la suivante : https://nexus.opendata-iledefrance.fr/ Vous pouvez vous logger via le bouton sign in en haut à droite de l’application avec votre couple utilisateur/mot de passe fourni par Atos. 

Une image contenant capture d’écran

 

Vous pouvez ensuite cliquer sur Browse pour voir la liste des repositories disponibles. 

VISUALISER LE DÉPOT DOCKER 

Une image contenant capture d’écran


Cliquez sur le dépôt docker-idfm vous aurez accès à l’ensemble du dépôt IDFM : 

 

Une image contenant capture d’écran



##VISUALISER LE DEPOT HELM 

Vous pouvez cliquer sur helm-idfm 

Une image contenant capture d’écran



Vous aurez accès à l’ensemble du dépôt helm : 

Une image contenant capture d’écran

 

Pour plus de détails vous pouvez consulter la documentation utilisateur de Nexus. 

 

#UTILISER DOCKER DEPUIS SON POSTE 

Dans un premier temps, authentifiez-vous sur le repository : 
```git
$ docker login 
```
 

Pour récupérer une image docker présente dans la repository Nexus depuis son poste, il faut faire cette commande : 
```git
$ docker pull nexus.opendata-iledefrance.fr/docker-opendata/NOM_IMAGE:TAG 
```
 

Pour envoyer une image, il faut faire cette commande : 
```git
$ docker tag NOM_IMAGE:TAG nexus.opendata-iledefrance.fr/NOM_IMAGE:TAG && \ 
```
```git
$ docker push nexus.opendata-iledefrance.fr/docker-opendata/NOM_IMAGE:TAG 
```
 