# Installation du proxy sur yum
dans le fichier /etc/yum.conf ajouter la ligne:
```
proxy=http://opendata-iledefrance.fr:3128
```
commande rapide:
```
sudo su && echo "proxy=http://opendata-iledefrance.fr:3128" >> /etc/yum.conf 
```

# Installation de docker 
Activer le repo docker-ce sur redhat:
```
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```

puis vous pourrez installer docker-ce: 
```
sudo yum -y install docker-ce
```
Activer le service docker au démarrage pour que docker soit dispo au redémarrage de la VM:
```
sudo systemctl enable docker
```
Dernière étape: démarrer tout de suite le démond docker pour y avoir accès de suite:
```
sudo systemctl start docker
```
Vous pouvez tester avec:
```
sudo docker ps
```

# Configurer les proxy sur docker
En root:
```
sudo mkdir /etc/systemd/system/docker.service.d;
sudo su && cat <<EOF > /etc/systemd/system/docker.service.d/http-proxy.conf
[Service]
Environment="HTTP_PROXY=http://opendata-iledefrance.fr:3128"
Environment="HTTPS_PROXY=http://opendata-iledefrance.fr:3128"
Environment="NO_PROXY=*.forge.opendata-iledefrance.fr"
EOF
```

# Définir l'IP de la carte réseau virtuelle

Optionnel, uniquement nécessaire pour ne pas entrer en collision avec l'adresse IP du LDAP

> **Note** : Attention, il a été remarqué que le changement ne prends pas tout le temps effet,
> si cela est le cas, jouer en modifiant une ligne dans le fichier, puis redémarrer docker à chaque modif,
> jusqu'à ce que le changement soit pris en compte

éditer le fichier daemon.json (il n'existe pas par défaut) :

```sh
sudo vim /etc/docker/daemon.json
```

insérer ceci :

```json
{
  "bip": "10.26.0.1/16"
}
```

Puis redémarrer docker:
```
sudo systemctl daemon-reload
sudo systemctl restart docker
```