# Installation de gitlab
Pour installer Gitlab (et les autres outils) il faut au préalable installer helm sur votre machine virtuelle.
## Installation helm:
Allez sur https://github.com/helm/helm/releases prendre la dernière relaese puis executer ces commandes
```
wget https://get.helm.sh/helm-v3.2.1-linux-arm64.tar.gz
tar -czvf helm-v3.2.1-linux-arm64.tar.gz
sudo cp ./helm /usr/bin/
chmod +x /usr/bin/helm
```

## Installation de gitlab avec helm:
### Créer le namespace:
```
kubectl create ns gitlab 
#On annote le namespace pour que tous les pods aient le nodeSelector master: (voir : https://stackoverflow.com/questions/52487333/how-to-assign-a-namespace-to-certain-nodes pour activer la fonction)
kubectl annotate ns gitlab scheduler.alpha.kubernetes.io/node-selector=node-role.kubernetes.io/master=
```

### version sans postgres:
```
helm upgrade -n gitlab git-idfm gitlab-3.3.5.tgz \
  --set global.edition=ce \
  --set global.hosts.domain=sources.gitlab.opendata-iledefrance.fr \
  --set global.hosts.externalIP=sources.gitlab.opendata-iledefrance.fr \
  --set global.hosts.gitlab.name=sources.gitlab.opendata-iledefrance.fr \
  --set global.hosts.minio.name=minio.gitlab.opendata-iledefrance.fr \
  --set global.time_zone=Europe/Paris \
  --set global.ingress.configureCertmanager=false \
  --set redis.metrics.enabled=false \
  --set postgresql.metrics.enabled=false \
  --set gitlab.webservice.ingress.enabled=false \
  --set gitlab.webservice.minReplicas=1 \
  --set gitlab.webservice.maxReplicas=1 \
  --set gitlab.gitlab-shell.minReplicas=1 \
  --set gitlab.gitlab-shell.maxReplicas=1 \
  --set gitlab.sidekiq.minReplicas=1 \
  --set gitlab.sidekiq.maxReplicas=1 \
  --set global.smtp.enabled=false \
  --set global.grafana.enabled=false \
  --set certmanager.install=false \
  --set nginx-ingress.enabled=false \
  --set prometheus.install=false \
  --set registry.enabled=false \
  --set gitlab-runner.install=false \
  --set global.gitaly.nodeSelector=node-role.kubernetes.io/master=\
  --set global.gitlab-exporter.nodeSelector=node-role.kubernetes.io/master=\
  --set global.gitlab-runner.nodeSelector=node-role.kubernetes.io/master=\
  --set global.sidekiq.nodeSelector=node-role.kubernetes.io/master=\
  --set global.gitlab-shell.nodeSelector=node-role.kubernetes.io/master=
  --set global.webservice.nodeSelector=node-role.kubernetes.io/master=

```
  ### Version avec postgres
  Il faudra avant créer le secret postgres
  ```
  helm install -n gitlab git-idfm gitlab-3.3.5.tgz \
  --set global.edition=ce \
  --set global.hosts.domain=sources.gitlab.opendata-iledefrance.fr \
  --set global.hosts.externalIP=sources.gitlab.opendata-iledefrance.fr \
  --set global.hosts.gitlab.name=sources.gitlab.opendata-iledefrance.fr \
  --set global.hosts.minio.name=minio.gitlab.opendata-iledefrance.fr \
  --set global.time_zone=Europe/Paris \
  --set global.ingress.configureCertmanager=false \
  --set redis.metrics.enabled=false \
  --set postgresql.metrics.enabled=false \
  --set gitlab.webservice.ingress.enabled=false \
  --set gitlab.webservice.minReplicas=1 \
  --set gitlab.webservice.maxReplicas=1 \
  --set gitlab.gitlab-shell.minReplicas=1 \
  --set gitlab.gitlab-shell.maxReplicas=1 \
  --set gitlab.sidekiq.minReplicas=1 \
  --set gitlab.sidekiq.maxReplicas=1 \
  --set global.smtp.enabled=false \
  --set global.grafana.enabled=false \
  --set certmanager.install=false \
  --set nginx-ingress.enabled=false \
  --set prometheus.install=false \
  --set registry.enabled=false \
  --set gitlab-runner.install=false \
  --set postgresql.install=false \
  --set global.psql.host=10.241.72.62 \
  --set global.psql.password.secret=gitlab-postgres-secret \
  --set global.psql.password.key=POSTGRES_PASSWORD \
  --set global.psql.database=gitlab \
  --set global.psql.username=gitlab_admin 
```

  ### Version avec postgres + ldap
  Il faudra avant créer le secret ldap
  ```
  helm install -n gitlab git-idfm gitlab-3.3.5.tgz \
  --set global.edition=ce \
  --set global.hosts.domain=sources.gitlab.opendata-iledefrance.fr \
  --set global.hosts.externalIP=sources.gitlab.opendata-iledefrance.fr \
  --set global.hosts.gitlab.name=sources.gitlab.opendata-iledefrance.fr \
  --set global.hosts.minio.name=minio.opendata-iledefrance.fr \
  --set global.time_zone=Europe/Paris \
  --set global.ingress.configureCertmanager=false \
  --set redis.metrics.enabled=false \
  --set postgresql.metrics.enabled=false \
  --set gitlab.webservice.ingress.enabled=false \
  --set gitlab.webservice.minReplicas=1 \
  --set gitlab.webservice.maxReplicas=1 \
  --set gitlab.gitlab-shell.minReplicas=1 \
  --set gitlab.gitlab-shell.maxReplicas=1 \
  --set gitlab.sidekiq.minReplicas=1 \
  --set gitlab.sidekiq.maxReplicas=1 \
  --set global.smtp.enabled=false \
  --set global.grafana.enabled=false \
  --set certmanager.install=false \
  --set nginx-ingress.enabled=false \
  --set prometheus.install=false \
  --set registry.enabled=false \
  --set gitlab-runner.install=false \
  --set postgresql.install=false \
  --set global.psql.host=10.241.72.62 \
  --set global.psql.password.secret=gitlab-postgres-secret \
  --set global.psql.password.key=POSTGRES_PASSWORD \
  --set global.psql.database=gitlab \
  --set global.psql.username=gitlab_admin 
  --set global.appConfig.ldap.enabled=true
  --set global.appConfig.ldap.servers.main.label=label
  --set global.appConfig.ldap.servers.main.host=host
  --set global.appConfig.ldap.servers.main.port=389
  --set global.appConfig.ldap.servers.main.uid=uid
  --set global.appConfig.ldap.servers.main.encryption=encryption
  --set global.appConfig.ldap.servers.main.verify_certificates=false
  --set global.appConfig.ldap.servers.main.active_directory=true
  --set global.appConfig.ldap.servers.main.base=base
  --set global.appConfig.ldap.servers.main.group_base=group_base
  --set global.appConfig.ldap.servers.main.admin_group=admin_group
  --set global.appConfig.ldap.servers.main.bind_dn=bind_dn
  --set global.appConfig.ldap.servers.main.password.secret=password_secret
  --set global.appConfig.ldap.servers.main.password.key=password_secret
  ```
  ### Installer le ingress:
creer le fichier gitlab-ingress.yaml contenant le contenu suivant: (creer le certificat au préalable)
  ```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: gitlab-ingress
  annotations:
    kubernetes.io/tls-acme: "true"
    kubernetes.io/ingress.class: "nginx"
    ingress.kubernetes.io/force-ssl-redirect: "true"
    nginx.ingress.kubernetes.io/proxy-body-size: "15m"
spec:
  rules:
  - host: sources.forge.iledefrance-mobilites.fr 
    http:
      paths:       
      - path: /
        backend:
          serviceName: git-antai-unicorn 
          servicePort: 8181
tls:
 - secretName: sources-tls # Secret for Tls certificat

  ```
  puis :
  ```
kubectl apply -f gitlab-ingress.yaml
  ```
